# USB Connector Web App

USB connector web app ini merupakan aplikasi yang dapat digunakan untuk mengatur konektivitas usb yang telah terhubung ke dalam suatu sistem. Kami memanfaatkan usb driver serta django dan html serta css framework untuk menyelesaikan proyek ini.

### Langkah Penggunaan
1. Siapkan virtual environment
2. Install dependency dari requirements.txt
3. Siapkan hardware flashdisk untuk pengujian
3. Run django server
4. Buka alamat server django (umumnya localhost)
5. Pilih device yang akan dibind/diunbind
6. Masukan password superuser yang benar (pada program ini password superuser yang dimasukkan perlu langsung benar, jika salah, maka perlu dilakukan handle pada command line karena command line akan meminta masukan password)
7. Tekan tombol bind/unbind untuk mengatur kontektivitas usb device.

Luqman Maulana Rizki - 1806205092
Ryan Karyadiputera - 1806205211
Tolhas Parulian Jonathan - 1806141473
