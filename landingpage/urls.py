from django.urls import path
from . import views

app_name = 'landingpage'

urlpatterns = [
    path('', views.index, name='landing'),
    path('<str:action>/<str:requested_bus>/', views.action, name='action_for_bus'),
]