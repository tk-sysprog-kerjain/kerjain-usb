from django.shortcuts import render, redirect
from subprocess import run, PIPE, Popen, STDOUT, check_call

def index(request):
    list_usb_command = ["bash","listusb.sh"]
    try:
        process = Popen(list_usb_command, stdout=PIPE, stderr=STDOUT)
        output = process.stdout.read()
        output = output.decode().split("\n")
        listdevice = []
        exitstatus = process.poll()
        for index in range(0, len(output)-1):
            item_splitted = output[index].split(": ")
            access_bus = item_splitted[0].split(" ")
            bus_identifier = access_bus[1]+'<separate>'+access_bus[3]
            device_name = item_splitted[1].split(" ")
            device_name = ' '.join(device_name[2:])
            device_id = item_splitted[1].split(" ")[1]
            device = {
                'name': device_name,
                'bus_identifier': bus_identifier,
                'id': device_id
            }
            listdevice.append(device)
        if (exitstatus==0):
            result = {"status": "Success", "output":str(output)}
        else:
            result = {"status": "Failed", "output":str(output)}
    except Exception as e:
        result =  {"status": "failed in python", "output":str(e)}
    return render(request, 'index.html', {'status': result['status'], 'devices': listdevice, 'output': result['output']})

def action(request, action='', requested_bus=''):
    try:
        if (action == 'bind'):
            if request.method == "POST":
                password = request.POST['bind_password']
                registration = Popen(['sudo', '-S', 'bash', 'usb-unbind.sh'], stdout=PIPE, stdin=PIPE)    
                bind_output = registration.communicate(input=str.encode(password))[0]
                requested_bus = requested_bus.replace("<separate>", "/")
                check_call("sudo bash ./usb-bind.sh '%s'" % requested_bus, shell=True)
        elif (action == 'unbind'):
            if request.method == "POST":
                password = request.POST['unbind_password']
                registration = Popen(['sudo', '-S', 'bash', 'usb-unbind.sh'], stdout=PIPE, stdin=PIPE)
                bind_output = registration.communicate(input=str.encode(password))[0]
                requested_bus = requested_bus.replace("<separate>", "/")
                check_call("sudo bash ./usb-unbind.sh '%s'" % requested_bus, shell=True)
    except Exception as e:
        return redirect(f"/")    
    return redirect(f"/")

#ref: https://stackoverflow.com/questions/51178003/run-bash-script-with-django    
