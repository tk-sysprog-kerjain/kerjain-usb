#!/bin/sh
device_loc_port=$(grep -l $1 /sys/bus/usb/devices/*/uevent) 
port=$(echo $device_loc_port | tr "/" " " | awk '{print $5}')
echo $port > /sys/bus/usb/drivers/usb/unbind



# reference: http://migueleonardortiz.com.ar/linux/learning-how-to-disable-specific-usb-devices-by-their-ports-in-linux/1645